package com.lara;
import org.apache.log4j.Logger;
public class Bootstrap
{
	final static Logger LOG = Logger.getLogger(Bootstrap.class);
public static void main(String[] args)
{
	LOG.trace("trace log");
	LOG.debug("Hello world Debug");
	LOG.info("info log");
	LOG.warn("warn log");
	LOG.error("error log");
	LOG.fatal("fatal log");
}
}
